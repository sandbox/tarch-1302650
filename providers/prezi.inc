<?php
// $Id: prezi.inc.txt,v 1.1 2010/10/20 18:45:11 alexua Exp $

/**
 * @file
 *  This is an prezi provider.
 */

/**
 *  This is the main URL for prezi.
 */
define('EMVIDEO_PREZI_MAIN_URL', 'http://www.prezi.com/');

/**
 *  This defines the version of the content data array that we serialize
 *  in emvideo_prezi_data(). If we change the expected keys of that array,
 *  we must increment this value, which will allow older content to be updated
 *  to the new version automatically.
 */
define('EMVIDEO_PREZI_DATA_VERSION', 1);

/**
 * hook emvideo_PROVIDER_info
 * This returns information relevant to a specific 3rd party video provider.
 *
 * @return
 *   A keyed array of strings requested by various admin and other forms.
 *    'provider' => The machine name of the provider. This must be the same as
 *      the base name of this filename, before the .inc extension.
 *    'name' => The translated name of the provider.
 *    'url' => The url to the main page for the provider.
 *    'settings_description' => A description of the provider that will be
 *      posted in the admin settings form.
 *    'supported_features' => An array of rows describing the state of certain
 *      supported features by the provider. These will be rendered in a table,
 *      with the columns being 'Feature', 'Supported', 'Notes'. In general,
 *      the 'Feature' column will give the name of the feature, 'Supported'
 *      will be Yes or No, and 'Notes' will give an optional description or
 *      caveats to the feature.
 */
function emvideo_prezi_info() {
  $features = array(
    array(t('Autoplay'), t('Yes'), ''),
    array(t('RSS Attachment'), t('No'), ''),
    array(t('Thumbnails'), t('No'), t('')),
    array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
  );
  return array(
    'provider' => 'prezi',
    'name' => t('Prezi'),
    'url' => EMVIDEO_PREZI_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !prezi.', array('!prezi' => l(t('Prezi.com'), EMVIDEO_PREZI_MAIN_URL))),
    'supported_features' => $features,
  );
}

/**
 *  hook emvideo_PROVIDER_settings
 *  This should return a subform to be added to the emvideo_settings() admin
 *  settings page.
 *
 *  Note that a form field set will already be provided at $form['prezi'],
 *  so if you want specific provider settings within that field set, you should
 *  add the elements to that form array element.
 */
function emvideo_prezi_settings() {
  // We'll add a field set of player options here. You may add other options
  // to this element, or remove the field set entirely if there are no
  // user-configurable options allowed by the prezi provider.
  $form['prezi']['player_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Embedded video player options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // This is an option to set the video to full screen. You should remove this
  // option if it is not provided by the prezi provider.
  $form['prezi']['player_options']['emvideo_prezi_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_prezi_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );
  $form['prezi']['player_options']['emvideo_prezi_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color of the object'),
    '#default_value' => variable_get('emvideo_prezi_color', 'ffffff'),
    '#description' => t('Define the object background color.'),
  );
  $form['prezi']['player_options']['emvideo_prezi_bgcolor'] = array(
    '#type' => 'textfield',
    '#title' => t('Presentation background color'),
    '#default_value' => variable_get('emvideo_prezi_bgcolor', '#ffffff'),
    '#description' => t('Define the background color of the presentation.'),
  );
  $form['prezi']['player_options']['emvideo_prezi_autohide_ctrls'] = array(
    '#type' => 'checkbox',
    '#title' => t('Autohide controls'),
    '#default_value' => variable_get('emvideo_prezi_autohide_ctrls', 0),
    '#description' => t('Define if the controls can be hidden.'),
  );
  return $form;
}

/**
 *  hook emvideo_PROVIDER_extract
 *
 *  This is called to extract the video code from a pasted URL or embed code.
 *
 *  We'll be passed a URL or the embed code from a video when an editor pastes
 *  that in the field's textfield. We'll need to either pass back an array of
 *  regex expressions to match, or do the matching ourselves and return the
 *  resulting video code.
 *
 *  @param $parse
 *    An optional string with the pasted URL or embed code.
 *  @return
 *    Either an array of regex expressions to be tested, or a string with the
 *    video code to be used. If the hook tests the code itself, it should
 *    return either the string of the video code (if matched), or an empty
 *    array. Otherwise, the calling function will handle testing the embed code
 *    against each regex string in the returned array.
 */
function emvideo_prezi_extract($parse = '') {
  // Here we assume that a URL will be passed in the form of
  // http://www.prezi.com/video/text-video-title
  // or embed code in the form of <object value="http://www.prezi.com/embed...".

  // We'll simply return an array of regular expressions for Embedded Media
  // Field to handle for us.
  return array(
    // In this expression, we're looking first for text matching the expression
    // between the @ signs. The 'i' at the end means we don't care about the
    // case. Thus, if someone enters http://www.Prezi.com, it will still
    // match. We escape periods as \., as otherwise they match any character.
    // The text in parentheses () will be returned as the provider video code,
    // if there's a match for the entire expression. In this particular case,
    // ([^?]+) means to match one more more characters (+) that are not a
    // question mark ([^\?]), which would denote a query in the URL.
    '@prezi\.com/([^\/]+)/@i'
  );
}

/**
 *  hook emvideo_PROVIDER_data
 *
 *  Provides an array to be serialised and made available with $item elsewhere.
 *
 *  This data can be used to store any extraneous information available
 *  specifically to the prezi provider.
 */
function emvideo_prezi_data($field, $item) {
  // Initialize the data array.
  $data = array();
  return $data;
}

/**
 * hook emvideo_PROVIDER_embedded_link($video_code)
 * returns a link to view the video at the provider's site.
 *  @param $video_code
 *    The string containing the video to watch.
 *  @return
 *    A string containing the URL to view the video at the original provider's site.
 */
function emvideo_prezi_embedded_link($video_code) {
  return 'http://www.prezi.com/'. $video_code;
}

/**
 *  hook emvideo_PROVIDER_video
 *  This actually displays the full/normal-sized video we want, usually on the
 *  default page view.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_prezi_video($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('prezi_flash', $item, $width, $height, $autoplay);
  return $output;
}

/**
 *  hook emvideo_PROVIDER_video
 *
 *  This actually displays the preview-sized video we want, commonly for the
 *  teaser.
 *  @param $embed
 *    The video code for the video to embed.
 *  @param $width
 *    The width to display the video.
 *  @param $height
 *    The height to display the video.
 *  @param $field
 *    The field info from the requesting node.
 *  @param $item
 *    The actual content from the field.
 *  @return
 *    The html of the embedded video.
 */
function emvideo_prezi_preview($embed, $width, $height, $field, $item, $node, $autoplay) {
  $output = theme('prezi_flash', $item, $width, $height, $autoplay);
  return $output;
}
