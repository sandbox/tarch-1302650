<?php

/**
 * The embedded flash displaying the prezi video.
 */
function theme_prezi_flash($item, $width, $height, $autoplay) {
  $output = '';
  if ($item['embed']) {
    $autoplay = $autoplay ? 'true' : 'false';
    $color = variable_get('emvideo_prezi_color', 'ffffff');
    $bgcolor = variable_get('emvideo_prezi_bgcolor', '#ffffff');
    $autohide_ctrls = variable_get('emvideo_prezi_autohide_ctrls', 0);
    $fullscreen = variable_get('emvideo_prezi_full_screen', 1) ? 'true' : 'false';
    $output = '<object id="prezi_' . $item['value'] . '" name="prezi_' . $item['value'] . '" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' . $width . '" height="' . $height . '">';
    $output .= '<param name="movie" value="http://prezi.com/bin/preziloader.swf"/>';
    $output .= '<param name="allowfullscreen" value="' . $fullscreen . '"/>';
    $output .= '<param name="allowscriptaccess" value="always"/>';
    $output .= '<param name="bgcolor" value="' . $bgcolor . '"/>';
    $output .= '<param name="flashvars" value="prezi_id=' . $item['value'] . '&amp;lock_to_path=0&amp;color=' . $color . '&amp;autoplay=no&amp;autohide_ctrls=' . $autohide_ctrls . '"/>';
    $output .= '<embed id="preziEmbed_' . $item['value'] . '" name="preziEmbed_' . $item['value'] . '" src="http://prezi.com/bin/preziloader.swf" type="application/x-shockwave-flash" allowfullscreen="' . $fullscreen . '" allowscriptaccess="always" width="' . $width . '" height="' . $height . '" bgcolor="' . $bgcolor . '" flashvars="prezi_id=' . $item['value'] . '&amp;lock_to_path=0&amp;color=' . $color . '&amp;autoplay=no&amp;autohide_ctrls=' . $autohide_ctrls . '"></embed>';
    $output .= '</object>';
  }
  return $output;
}
